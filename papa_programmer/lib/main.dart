import 'package:flutter/material.dart';
import 'package:papa_programmer/onboarding/onboarding_1.dart';
import 'package:papa_programmer/screens/course_detail.dart';
import 'package:papa_programmer/screens/tabs_bottomNavBar.dart';
import 'package:papa_programmer/screens/topic_detail_screen.dart';
import 'package:papa_programmer/screens/tutorial_detail_screen.dart';
import 'package:papa_programmer/screens/tutorials_screen.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './onboarding/onboarding_1.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

int initScreen = 0;
Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences preferences = await SharedPreferences.getInstance();
  initScreen = preferences.getInt('initScreen');
  await preferences.setInt('initScreen', 1);
  await Firebase.initializeApp();
  MobileAds.instance.initialize();

  runApp(
    MyApp(),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Papa Programmer',
      theme: ThemeData(
        // primarySwatch: Color(0xFC0444),
        primaryColor: Color(0xFFFC0444),
        fontFamily: 'Nexa',
      ),
      initialRoute: initScreen == 0 || initScreen == null
          // ? OnboardingScreen()
          ? OnboardingScreen.routeName
          : HomeScreen.routeName,
      routes: {
        OnboardingScreen.routeName: (ctx) => OnboardingScreen(),
        HomeScreen.routeName: (ctx) => HomeScreen(),
        CourseDetail.routeName: (ctx) => CourseDetail(),
        TopicDetailScreen.routeName: (ctx) => TopicDetailScreen(),
        TutorialScreen.routeName: (ctx) => TutorialScreen(),
        TutorialDetailScreen.routeName: (ctx) => TutorialDetailScreen(),
      },
    );
  }
}

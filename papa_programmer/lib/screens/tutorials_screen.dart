import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:papa_programmer/widgets/tutorial_box.dart';

class TutorialScreen extends StatefulWidget {
  static const routeName = '/tutorial';

  @override
  _TutorialScreenState createState() => _TutorialScreenState();
}

class _TutorialScreenState extends State<TutorialScreen> {
  // TextEditingController _searchController = TextEditingController();
  final database = FirebaseFirestore.instance;
  String searchString = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tutorials"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 25),
              child: StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestore.instance
                    .collection("tutorials")
                    .orderBy('id', descending: true)
                    .snapshots(),
                builder: (context, snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    default:
                      return Column(
                        children: [
                          ListView.builder(
                            shrinkWrap: true,
                            itemBuilder: (ctx, index) {
                              DocumentSnapshot documentSnapshot =
                                  snapshot.data.docs[index];
                              return Container(
                                child: Column(
                                  children: [
                                    TutorialBox(
                                      title: documentSnapshot['heading'],
                                      language: documentSnapshot['language'],
                                      code: documentSnapshot['code'],
                                      githubLink:
                                          documentSnapshot['githubLink'],
                                    ),
                                  ],
                                ),
                              );
                            },
                            itemCount: snapshot.data.docs.length,
                          ),
                        ],
                      );
                  }
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

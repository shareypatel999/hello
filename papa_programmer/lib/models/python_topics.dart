import 'package:flutter/material.dart';

class PythonTopics {
  final String heading;
  final List paragraphList;

  PythonTopics({
    @required this.heading,
    @required this.paragraphList,
  });
}

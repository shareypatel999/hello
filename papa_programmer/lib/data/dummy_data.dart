import '../models/course_model.dart';

const DUMMY_DATA = const [
  Course(
    id: '1',
    courseName: "Python",
    courseLogo:
        "https://firebasestorage.googleapis.com/v0/b/papaprogrammer-65ab8.appspot.com/o/pythonLogo%20(1).png?alt=media&token=256f15ed-af6c-4372-a127-60c63ad739eb",
    courseSubtitle: 'Learn fundamentals\nof python',
  ),
];

import 'package:flutter/material.dart';
import 'package:papa_programmer/adhelper.dart';
import 'package:syntax_highlighter/syntax_highlighter.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class TutorialDetailScreen extends StatefulWidget {
  static const routeName = '/tutorial_detail_screen';

  @override
  _TutorialDetailScreenState createState() => _TutorialDetailScreenState();
}

class _TutorialDetailScreenState extends State<TutorialDetailScreen> {
  @override
  Widget build(BuildContext context) {
    final SyntaxHighlighterStyle style =
        Theme.of(context).brightness == Brightness.dark
            ? SyntaxHighlighterStyle.darkThemeStyle()
            : SyntaxHighlighterStyle.lightThemeStyle();

    final tutorialData = ModalRoute.of(context).settings.arguments as Map;

    return Scaffold(
      appBar: AppBar(
        title: Text(tutorialData['title']),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: InteractiveViewer(
                child: Container(
                  width: double.infinity,
                  child: RichText(
                    text: TextSpan(
                      style: const TextStyle(
                          fontFamily: 'monospace', fontSize: 10.0),
                      children: <TextSpan>[
                        DartSyntaxHighlighter(style).format(
                            tutorialData['code'].replaceAll('NEW_LINE', '\n')),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: 130,
              height: 45,
              decoration: BoxDecoration(
                color: Colors.black87,
                borderRadius: BorderRadius.circular(10),
              ),
              child: InkWell(
                onTap: () {
                  launch(tutorialData['githubLink']);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      icon: FaIcon(
                        FontAwesomeIcons.github,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5.0),
                      child: Text(
                        'Github',
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: Container(
        height: 80,
        child: AdWidget(
          ad: AdmobHelper.getBannerAd()..load(),
          key: UniqueKey(),
        ),
      ),

      // body: Container(
      //   child: AdWidget(
      //     ad: AdmobHelper.getBannerAd()..load(),
      //     key: UniqueKey(),
      //   ),
      // ),
    );
  }
}

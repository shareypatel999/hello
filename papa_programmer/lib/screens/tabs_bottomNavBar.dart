import 'package:flutter/material.dart';
// import 'package:papa_programmer/screens/search_screen.dart';
import 'package:papa_programmer/screens/tutorials_screen.dart';
import 'courses_home.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/tab_screen';
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int _currentIndex = 0;
  final tabs = [
    CourseMainScreen(),
    TutorialScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedFontSize: 15,
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: Color(0xFFFC043F).withOpacity(0.7),
            ),
            activeIcon: Icon(
              Icons.home,
              color: Color(0xFFFC043F),
              size: 30,
            ),
            title: Text("Home", style: TextStyle(color: Colors.black)),
          ),
          BottomNavigationBarItem(
            icon: Icon(
              Icons.book,
              color: Color(0xFFFC043F).withOpacity(0.7),
            ),
            activeIcon: Icon(
              Icons.book,
              color: Color(0xFFFC043F),
              size: 30,
            ),
            title: Text(
              "Tutorials",
              style: TextStyle(color: Colors.black),
            ),
          ),
        ],
        onTap: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
      ),
    );
  }
}

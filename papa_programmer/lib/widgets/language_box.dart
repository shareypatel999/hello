import 'package:flutter/material.dart';
// import 'package:papa_programmer/models/course_model.dart';
import 'package:papa_programmer/screens/course_detail.dart';
// import '../data/dummy_data.dart';

class LanguageBox extends StatelessWidget {
  final String courseTitle;
  final String courseSubtitle;
  final String courseImage;
  // final String id;
  // final List topics;

  LanguageBox({
    @required this.courseTitle,
    @required this.courseSubtitle,
    @required this.courseImage,
    // required this.id,
    // required this.topics,
  });

  void selectCourse(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      CourseDetail.routeName,
      arguments: {
        'courseTitle': courseTitle,
        'courseImage': courseImage,
        // 'topics': topics,
        // 'id': id,
        'courseSubtitle': courseSubtitle,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 18.0, right: 18, left: 18),
      child: InkWell(
        splashColor: Color(0xFFFC043F),
        onTap: () => selectCourse(context),
        child: Container(
          decoration: BoxDecoration(
            color: Color(0xFFFC043F).withOpacity(1),
            borderRadius: BorderRadius.all(
              Radius.circular(12),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 5.0,
                offset: Offset(
                  5.0,
                  5.0,
                ),
              ),
            ],
          ),
          width: double.infinity,
          height: 125,
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      courseTitle.toUpperCase(),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0),
                      child: Text(
                        courseSubtitle,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.white.withOpacity(0.9),
                          // fontStyle: FontStyle.italic,
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Image.network(
                    courseImage,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

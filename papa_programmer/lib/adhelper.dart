import 'package:google_mobile_ads/google_mobile_ads.dart';

class AdmobHelper {
  static String get bannerUnit => 'ca-app-pub-6224283006444128/5780835671';

  static intialization() {
    if (MobileAds.instance == null) {
      MobileAds.instance.initialize();
    }
  }

// test Ad: ca-app-pub-3940256099942544/6300978111
// Admob ad: ca-app-pub-6224283006444128/5780835671
  static BannerAd getBannerAd() {
    BannerAd bAd = new BannerAd(
      size: AdSize.banner,
      adUnitId: 'ca-app-pub-6224283006444128/5780835671',
      request: AdRequest(),
      listener: BannerAdListener(
        onAdLoaded: (Ad ad) => print('Ad loaded'),
        onAdFailedToLoad: (Ad ad, LoadAdError error) {
          ad.dispose();
        },
        onAdOpened: (Ad ad) => print('Ad opened'),
        onAdClosed: (Ad ad) => print('Ad closed!'),
      ),
    );

    return bAd;
  }
}

import 'package:flutter/material.dart';
import 'package:papa_programmer/screens/topic_detail_screen.dart';
import '../data/python/topics.dart';

class CourseDetail extends StatelessWidget {
  static const routeName = '/course';

  // final String courseTitle;
  // CourseDetail({
  //   required this.courseTitle,
  // });

  final String paragraph = 'Introduction';

  void selectTopic(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      TopicDetailScreen.routeName,
      arguments: {
        'paragraph': paragraph,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final courseData =
        ModalRoute.of(context).settings.arguments as Map<String, Object>;
    return Scaffold(
      appBar: AppBar(
        title: Text(courseData['courseTitle'].toString()),
      ),
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: SingleChildScrollView(
          physics: ScrollPhysics(),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Image.network(
                  courseData['courseImage'].toString(),
                  width: MediaQuery.of(context).size.width * 0.5,
                  alignment: Alignment.centerRight,
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  top: 30,
                  left: 15,
                  right: 15,
                ),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20.0),
                    color: Colors.white,
                  ),
                  width: double.infinity,
                  height: 500,
                  // color: Colors.white,
                  child: SingleChildScrollView(
                    child: Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: Column(
                        children: [
                          ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemBuilder: (ctx, index) {
                              return TopicContainer(
                                heading: pythonTopics[index].heading,
                                // paragraph: pythonTopics[index].paragraph,
                                paragraphList:
                                    pythonTopics[index].paragraphList,
                              );
                            },
                            itemCount: pythonTopics.length,
                          ),
                          // Text(
                          //   courseData['courseTitle'],
                          // ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TopicContainer extends StatelessWidget {
  final String heading;
  // final String paragraph;
  final List paragraphList;
  // final Map paragraphMap;
  // final List paragraphMapList;

  TopicContainer({
    @required this.heading,
    // @required this.paragraph,
    @required this.paragraphList,
    // required this.paragraphMap,
    // required this.paragraphMapList,
  });

  void selectTopic(BuildContext ctx) {
    Navigator.of(ctx).pushNamed(
      TopicDetailScreen.routeName,
      arguments: {
        'heading': heading,
        // 'paragraph': paragraph,
        'paragraphList': paragraphList,
        // 'paragraphMap': paragraphMap,
        // 'paragraphMapList': paragraphMapList,
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectTopic(context),
      child: Padding(
        padding: const EdgeInsets.only(
          top: 7.5,
          bottom: 7.5,
        ),
        child: Container(
          child: Padding(
            padding: const EdgeInsets.only(
              top: 1,
              left: 15,
              right: 15,
            ),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
                color: Theme.of(context).primaryColor.withOpacity(0.2),
              ),
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 15),
                child: Text(
                  heading.toString(),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

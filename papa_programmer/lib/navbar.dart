import 'package:flutter/material.dart';
// import 'package:papa_programmer/screens/course_detail.dart';
import 'package:papa_programmer/screens/tabs_bottomNavBar.dart';
import 'dart:io';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:papa_programmer/screens/tutorials_screen.dart';

class NavBar extends StatelessWidget {
  launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: [
          Container(
            height: 200,
            color: Theme.of(context).primaryColor,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 15, left: 15),
              child: Text(
                'Hello 👋\nCoder!!',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            alignment: Alignment.bottomLeft,
          ),
          Divider(
            thickness: 1,
          ),
          ListTile(
            leading: Icon(
              Icons.home,
              color: Theme.of(context).primaryColor,
            ),
            title: Text('Home'),
            onTap: () {
              Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
            },
          ),
          ListTile(
            leading: Icon(Icons.book),
            title: Text('Tutorials'),
            onTap: () {
              Navigator.of(context).pushNamed(TutorialScreen.routeName);
            },
          ),
          ListTile(
            leading: FaIcon(FontAwesomeIcons.instagram),
            title: Text('Instagram'),
            onTap: () {
              launchURL('https://www.instagram.com/papa_programmer/');
            },
          ),
          Divider(
            thickness: 1,
          ),
          ListTile(
            leading: Icon(Icons.exit_to_app),
            title: Text('Exit'),
            onTap: () {
              exit(0);
            },
          ),
          Divider(
            thickness: 1,
          ),
        ],
      ),
    );
  }
}

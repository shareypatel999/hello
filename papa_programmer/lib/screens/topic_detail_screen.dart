import 'package:flutter/material.dart';

class TopicDetailScreen extends StatelessWidget {
  static const routeName = '/topic_detail_screen';

  // final String paragraph;
  // final List paragraphList;
  // final Map paragraphMap;
  // final List paragraphMapList;

  // TopicDetailScreen({
  //   required this.paragraph,
  //   required this.paragraphList,
  //   required this.paragraphMap,
  //   required this.paragraphMapList,
  // });

  @override
  Widget build(BuildContext context) {
    final topicData = ModalRoute.of(context).settings.arguments as Map;
    // final dict = topicData['paragraphMap'];
    // print(topicData['paragraphMapList']);
    print(topicData['paragraphMap']);

    // var usrMap = {"name": "Tom", 'Email': 'tom@xyz.com'};
    // topicData['paragraphMap'].forEach((k, v) => Text('${k}: ${v}'));
    // usrMap.forEach((k, v) => print('${k}: ${v}'));

    // print(dict.keys);
    // print(topicData['paragraphMap'].values);
    return Scaffold(
      appBar: AppBar(
        title: Text(topicData['heading']),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                  // child: Text(topicData['paragraph']),
                  ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 18.0, left: 10, right: 10, bottom: 24),
                child: Column(
                  children: [
                    for (var i in topicData['paragraphList'])
                      Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            i.toString(),
                            style: TextStyle(fontSize: 18),
                          )),
                    // topicData['paragraphMap'].forEach((k, v) => Text(k)),

                    // ...topicData['paragraphList'].map((tx) {
                    //   print(tx);
                    //   return Text('Hello');
                    // }).toList(),

                    // Text(topicData['paragraphMap'].value),

                    // ...(topicData['paragraphList'] as List).map((element) {
                    //   return Text(element);
                    // }).toList(),

                    // Text(dict['h'].toString()),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

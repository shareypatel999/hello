import 'package:flutter/material.dart';
import 'package:papa_programmer/data/dummy_data.dart';
import '../widgets/language_box.dart';
import '../navbar.dart';

class CourseMainScreen extends StatefulWidget {
  @override
  _CourseMainScreenState createState() => _CourseMainScreenState();
}

class _CourseMainScreenState extends State<CourseMainScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Papa Programmer'),
      ),
      drawer: NavBar(),
      body: SafeArea(
        child: Column(
          children: [
            Row(
              children: <Widget>[],
            ),
            ListView.builder(
              shrinkWrap: true,
              itemBuilder: (ctx, index) {
                return LanguageBox(
                  courseTitle: DUMMY_DATA[index].courseName,
                  courseSubtitle: DUMMY_DATA[index].courseSubtitle,
                  courseImage: DUMMY_DATA[index].courseLogo,
                  // id: DUMMY_DATA[index].id,
                );
              },
              itemCount: DUMMY_DATA.length,
            ),
            TemporaryBox(),
          ],
        ),
      ),
    );
  }
}

class TemporaryBox extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 18.0, right: 18, left: 18),
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xFFFC043F).withOpacity(1),
          borderRadius: BorderRadius.all(
            Radius.circular(12),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 5.0,
              offset: Offset(
                5.0,
                5.0,
              ),
            ),
          ],
        ),
        width: double.infinity,
        height: 125,
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Very Soon...".toUpperCase(),
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 3.0),
                      child: Text(
                        'HTML, CSS, JavaScript,\nReact, Flutter, C, C++',
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.white.withOpacity(0.9),
                          // fontStyle: FontStyle.italic,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Image.network(
                  'https://firebasestorage.googleapis.com/v0/b/papaprogrammer-65ab8.appspot.com/o/languages_logo.png?alt=media&token=d664f8d3-d7a6-45ad-9a72-9a28da3ad729',
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:papa_programmer/screens/tabs_bottomNavBar.dart';
// import 'package:shared_preferences/shared_preferences.dart';

const kTitleStyle = TextStyle(
  fontSize: 30,
  color: Color(0xFFFC0444),
  fontWeight: FontWeight.bold,
);
const kSubtitleStyle = TextStyle(
  fontSize: 18,
  color: Colors.black87,
);

class OnboardingScreen extends StatefulWidget {
  static const routeName = '/onboarding_screen';

  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  final pageController = new PageController(initialPage: 0);

  void nextPage() {
    pageController.nextPage(
        duration: const Duration(milliseconds: 300), curve: Curves.ease);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          child: PageView(
            physics: NeverScrollableScrollPhysics(),
            controller: pageController,
            children: [
              Slide(
                hero: Image.asset('./assets/images/code.png'),
                heading: 'Learn Programming',
                subheading:
                    'Learn how to code in the most efficient way with short and simple programming content.',
                onNext: nextPage,
              ),
              Slide(
                hero: Image.asset('./assets/images/pro.png'),
                heading: 'Coding Tutorials',
                subheading:
                    'Learn new programming concepts from frequently posted coding tutorials.',
                onNext: nextPage,
              ),
              Slide(
                hero: Image.asset('./assets/images/lang.png'),
                heading: 'Coming Soon...',
                subheading:
                    'Learn different programming languages and libraries like JavaScript, C, C++, React, etc.,',
                onNext: nextPage,
              ),
              HomeScreen(),
            ],
          ),
        ),
      ),
    );
  }
}

class Slide extends StatelessWidget {
  final Widget hero;
  final String heading;
  final String subheading;
  final VoidCallback onNext;

  Slide({
    @required this.hero,
    @required this.heading,
    @required this.subheading,
    @required this.onNext,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            child: Padding(
              padding: const EdgeInsets.all(18.0),
              child: hero,
            ),
          ),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: Column(
              children: [
                Text(
                  heading,
                  style: kTitleStyle,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),
                Text(
                  subheading,
                  style: kSubtitleStyle,
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 25),
                Padding(
                  padding: const EdgeInsets.only(right: 35.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Container(
                        // alignment: Alignment.centerRight,
                        decoration: BoxDecoration(
                          color: Color(0xFFFC0444),
                          borderRadius: BorderRadius.circular(50),
                        ),
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                          onPressed: onNext,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

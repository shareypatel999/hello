import 'package:flutter/material.dart';
import 'package:papa_programmer/screens/tutorial_detail_screen.dart';

class TutorialBox extends StatelessWidget {
  final String title;
  final String language;
  final String code;
  final String githubLink;

  TutorialBox(
      {@required this.title,
      @required this.language,
      @required this.code,
      @required this.githubLink});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18.0, vertical: 8),
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed(
            TutorialDetailScreen.routeName,
            arguments: {
              'title': title,
              'language': language,
              'code': code,
              'githubLink': githubLink,
            },
          );
        },
        child: Container(
          width: double.infinity,
          height: 100,
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        child: Text(
                          title,
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                          maxLines: 5,
                          // softWrap: false,
                          // overflow: TextOverflow.fade,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 25),
                        child: Text(
                          language,
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';

class Course {
  final String id;
  final String courseName;
  final String courseSubtitle;
  final String courseLogo;

  const Course({
    @required this.id,
    @required this.courseName,
    @required this.courseSubtitle,
    @required this.courseLogo,
  });
}
